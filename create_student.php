<?php
$servename = 'localhost';
$username = 'root';
$password = 'root';
$database = 'dbmgt';
$port=3306;
$conn = new mysqli($servername, $username, $password, $database, $port);
if ($conn->connect_error) { 
    die(''. $conn->connect_error);
}
$email =$_POST['email'];
$password =$_POST['password'];
$sql = 'INSERT INTO students(email,password)VALUES(?,?)';
$stmt= $conn->prepare($sql);
$stmt->bind_param('ss', $email,$password);
if ($stmt ->execute()) {
    echo 'New record inserted successfully'."<br>";
}
else {
    echo "Error:".$sql."<br>".$conn->error."<br>";
}
$stmt->close();

$conn->close();